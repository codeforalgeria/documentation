<img src="assets/logo.png" width="200px" align="middle">

# Code for Algeria
We are a **non-profit organization**, with the idea of programming in our free time for causes that will help people and government companies to facilitate their day-to-day with computerized solutions.

## Founders:
It's an initiative of our teacher <a href="https://www.univ-constantine2.dz/blog/profil-enseignant/kitouni-ilham/">Mrs. Kitouni</a> and it's students.
## Collaborators:
For now there are a few people working in their spare time, but if you want to collaborate with us please send us an email for approval you can join us here: <a href="mailto:codeforalgeria@gmail.com">codeforalgeria@gmail.com</a> or join our <a href="https://join.slack.com/t/codeforalgeria/shared_invite/enQtNTIyNDkzNzY1OTg3LWVhYjJmMWVjZDgzNzViYzQ5NzdkMDA2ZDkyMDAzYmQwOGZmYzYyMWVkOGZlNTQzOTE4NTg5N2E4ODIxZmEwOGU">#slack</a>.
## Contributing:
Before contributing make sure to read this <a href="https://gitlab.com/codeforalgeria/documentation/blob/master/CONTRIBUTING.md">CONTRIBUTING</a>.
### Credits:
Logo designed by: <a href="https://www.linkedin.com/in/mohyiddine-dilmi-aa987611b/">Dilmi Moheyeddin</a>.

## License: