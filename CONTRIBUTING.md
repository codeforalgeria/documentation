# CONTRIBUTING:

Make sure that your master branch always is up-to-date. Select one the `TODO` list and comment that you will work on that features.
```
git fetch
git pull origin master
```
When updating your current branch and want to add some **new features** you must work on the `features` branch and create a new branch:
```
git checkout -b features-name-of-the-feature features
```
and then you can switch to your new branch:
```
git checkout features-name-of-the-feature
```
If you want to merge you can:
```
git push origin features/features-name-of-the-feature
```
and then make a pull request on gitlab.

To delete your branch:
```
git branch -d features-name-of-the-feature
```
